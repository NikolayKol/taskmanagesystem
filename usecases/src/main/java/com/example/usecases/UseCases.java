package com.example.usecases;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;

import com.example.entities.Model;
import com.example.entities.Task;
import com.example.entities.TaskItem;
import com.example.udpconnection.Change;
import com.example.udpconnection.Connection;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import static com.example.udpconnection.Connection.EVENTS_CHANGE;

/**
 * Created by nikolay on 18.02.17.
 */

public class UseCases {

    public static final int ACTIVE_TASKS_UPDATED_MESSAGE_CODE = 121;

    private static final String PREFS_NAME = "prefs";
    private static final String TABLET_ID_KEY = "id";

    private Model model;
    private String tabletId;
    private Handler handler;
    private List<Task> activeTasks;
    private Connection connection;

    public UseCases(Context context) {
        activeTasks = new ArrayList<>();
        loadModel(context);
        loadTabletId(context);
        connection = new Connection(tabletId, connectionCallback);
    }

    public void setUiCallback(Handler.Callback callback) {
        this.handler = new Handler(callback);
    }

    public void removeUiCallback() {
        this.handler = null;
    }



    public List<Task> getActiveTasks() {
        return activeTasks;
    }

    public Model getModel() {
        return model;
    }


    public void initTask(List<String> personsIds, List<String> actionsIds, List<String> locationsIds) {
        Task task = new Task(UUID.randomUUID().toString(), tabletId, new Date().getTime(), personsIds, actionsIds, locationsIds);
        connection.initTask(task);
    }

    public void acknowledgeTask(Task task) {
        connection.acknowledgeTask(task);
    }

    public void acknowledgeAllTasks() {
        for (Task task : activeTasks) {
            connection.acknowledgeTask(task);
        }
    }



    private void loadModel(Context context) {
        Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.entities));
        StringBuilder builder = new StringBuilder();

        while (scanner.hasNext()) {
            builder.append(scanner.next());
        }

        this.model = new Gson().fromJson(builder.toString(), Model.class);
    }

    private void loadTabletId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String tabletId = preferences.getString(TABLET_ID_KEY, "");

        if (tabletId.isEmpty()) {
            tabletId = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(TABLET_ID_KEY, tabletId);
            editor.commit();
        }

        this.tabletId = tabletId;
    }

    private void refreshActiveTasks(Change change) {
        activeTasks.removeAll(change.getRemovedTasks());
        activeTasks.addAll(change.getAddedTasks());
        Message message = new Message();
        message.what = ACTIVE_TASKS_UPDATED_MESSAGE_CODE;
        handler.sendMessage(message);
    }

    private static String generateTestData() {

        List<TaskItem> persons = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            persons.add(new TaskItem("person" + i, UUID.randomUUID().toString()));
        }

        List<TaskItem> actions = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            actions.add(new TaskItem("action" + i, UUID.randomUUID().toString()));
        }

        List<TaskItem> locations = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            locations.add(new TaskItem("location" + i, UUID.randomUUID().toString()));
        }

        Model model = new Model(persons, actions, locations);

        return new Gson().toJson(model);
    }

    private Handler.Callback connectionCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case EVENTS_CHANGE:
                    Change change = (Change) msg.obj;

                    if (change != null) refreshActiveTasks(change);

                    return true;
                default:
                    return false;
            }
        }
    };
}
