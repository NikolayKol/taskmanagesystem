package com.example.udpconnection;

import com.example.entities.Task;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nikolay on 04.03.17.
 */

public class DeviceEventsState {

    @SerializedName("device_id")
    private String deviceId;

    @SerializedName("initiated_tasks")
    private List<Task> initiatedTasks;

    @SerializedName("acknowledges_tasks")
    private List<Task> acknowledgesTasks;

    private transient Gson gson;

    public DeviceEventsState(String deviceId) {
        this.deviceId = deviceId;
        initiatedTasks = new ArrayList<>();
        acknowledgesTasks = new ArrayList<>();
        gson = new Gson();
    }

    public String getDeviceId() {
        return deviceId;
    }

    public List<Task> getInitiatedTasks() {
        return initiatedTasks;
    }

    public List<Task> getAcknowledgesTasks() {
        return acknowledgesTasks;
    }

    public synchronized String getJsonString() {
        return gson.toJson(this);
    }

    public synchronized void initTask(Task task) {
        initiatedTasks.add(task);
    }

    public synchronized void acknowledgeTask(Task task) {
        acknowledgesTasks.add(task);
    }

    public synchronized void applyReceivedDeviceEventsState(DeviceEventsState deviceEventsState) {

        //remove initiated tasks, acknowledged from received device
        Iterator<Task> initiatedTasksIterator = initiatedTasks.iterator();
        while (initiatedTasksIterator.hasNext()) {
            Task initiatedTask = initiatedTasksIterator.next();
            if (deviceEventsState.getAcknowledgesTasks().contains(initiatedTask))
                initiatedTasksIterator.remove();
        }


        //remove tasks from acknowledges
        String receivedDeviceId = deviceEventsState.getDeviceId();
        Iterator<Task> acknowledgesTasksIterator = acknowledgesTasks.iterator();
        while (acknowledgesTasksIterator.hasNext()) {
            Task acknowledgedTask = acknowledgesTasksIterator.next();
            if (acknowledgedTask.getInitiatorId().equals(receivedDeviceId) && !deviceEventsState.getInitiatedTasks().contains(acknowledgedTask))
                acknowledgesTasksIterator.remove();
        }
    }
}
