package com.example.udpconnection;

/**
 * Created by nikolay on 04.03.17.
 */

public class Constants {

    public static final int LISTEN_PORT = 7789;
    public static final int SEND_PORT = 7777;


    public static final long SEND_STATE_PERIOD_MS = 100;
}
