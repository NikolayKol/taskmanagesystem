package com.example.udpconnection;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.entities.Task;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nikolay on 04.03.17.
 */

public class Connection {

    public static final int EVENTS_CHANGE = 9988;

    private static final String LOG_TAG = "connection";

    private Handler handler;
    private DeviceEventsState deviceState;
    private List<Task> activeTasks;

    public Connection(String deviceId, Handler.Callback connectionCallback) {
        this.handler = new Handler(connectionCallback);
        this.deviceState = new DeviceEventsState(deviceId);
        this.activeTasks = new ArrayList<>();
        new Thread(new SendRunnable()).start();
        new Thread(new ListenRunnable()).start();
    }

    public void initTask(Task task) {
        deviceState.initTask(task);
    }

    public void acknowledgeTask(Task task) {
        deviceState.acknowledgeTask(task);
    }

    private class SendRunnable implements Runnable {
        @Override
        public void run() {
            DatagramSocket s = null;
            try {
                s = new DatagramSocket(Constants.SEND_PORT);
                s.setBroadcast(true);

                while (true) {
                    String deviceStateJson = deviceState.getJsonString();
                    byte[] buffer = deviceStateJson.getBytes();
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length, Inet4Address.getByName("255.255.255.255"), Constants.LISTEN_PORT);
                    s.send(packet);
                    Log.d(LOG_TAG, "send device state");

                    try {
                        Thread.sleep(Constants.SEND_STATE_PERIOD_MS);
                    }
                    catch (InterruptedException ignored){}
                }
            }
            catch (IOException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }

    private class ListenRunnable implements Runnable {

        private Gson gson = new Gson();

        @Override
        public void run() {
            DatagramSocket s = null;

            try {
                s = new DatagramSocket(Constants.LISTEN_PORT, Inet4Address.getByName("0.0.0.0"));
                s.setBroadcast(true);

                while (true) {
                    byte[] buffer = new byte[16384];

                    DatagramPacket recvPacket = new DatagramPacket(buffer, buffer.length);

                    s.receive(recvPacket);

                    String recvData = new String(recvPacket.getData());
                    Log.d(LOG_TAG, "received data = " + recvData);

                    JsonReader jsonReader = new JsonReader(new StringReader(recvData));
                    jsonReader.setLenient(true);

                    DeviceEventsState receivedDeviceState = gson.fromJson(jsonReader, DeviceEventsState.class);

                    deviceState.applyReceivedDeviceEventsState(receivedDeviceState);

                    Change change = handleReceivedData(receivedDeviceState);

                    if (change == null) continue;


                    Message message = new Message();
                    message.what = EVENTS_CHANGE;
                    message.obj = change;
                    handler.sendMessage(message);
                }

            }
            catch (IOException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }

        private Change handleReceivedData(DeviceEventsState receivedDeviceState) {
            Change change = null;

            for (Task task : receivedDeviceState.getInitiatedTasks()) {
                if (!activeTasks.contains(task)) {
                    activeTasks.add(task);

                    if (change == null) change = new Change();

                    change.addTask(task);
                }
            }

            String receivedDeviceId = receivedDeviceState.getDeviceId();
            Iterator<Task> iterator = activeTasks.iterator();
            while (iterator.hasNext()) {
                Task task = iterator.next();
                if (task.getInitiatorId().equals(receivedDeviceId) && !receivedDeviceState.getInitiatedTasks().contains(task)) {
                    iterator.remove();
                    if (change == null) change = new Change();
                    change.removeTask(task);
                }
            }

            return change;
        }
    }
}
