package com.example.udpconnection;

import com.example.entities.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikolay on 04.03.17.
 */

public class Change {

    private List<Task> addedTasks;
    private List<Task> removedTasks;

    public Change() {
        addedTasks = new ArrayList<>();
        removedTasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        addedTasks.add(task);
    }

    public void removeTask(Task task) {
        removedTasks.add(task);
    }

    public List<Task> getAddedTasks() {
        return addedTasks;
    }

    public List<Task> getRemovedTasks() {
        return removedTasks;
    }
}
