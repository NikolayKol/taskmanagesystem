package politex.com.taskmanagesystem.main_activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import politex.com.taskmanagesystem.R;
import static com.example.usecases.UseCases.ACTIVE_TASKS_UPDATED_MESSAGE_CODE;

import politex.com.taskmanagesystem.TaskManageApp;
import politex.com.taskmanagesystem.views.CollapseButton;
import politex.com.taskmanagesystem.views.TaskItemsView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.task_items_view) TaskItemsView taskItemsView;
    @BindView(R.id.collapse_button) CollapseButton collapseButton;
    @BindView(R.id.tasks_recycler_view) RecyclerView tasksRecyclerView;

    private TasksAdapter tasksAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tasksAdapter = new TasksAdapter();
        tasksRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        tasksRecyclerView.setAdapter(tasksAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        collapseButton.updateTasksCount();
        TaskManageApp.getUseCases().setUiCallback(callback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        TaskManageApp.getUseCases().removeUiCallback();
    }

    @OnClick(R.id.collapse_button)
    void collapse() {
        if (taskItemsView.isCollapsed()) {
            taskItemsView.expand();
            collapseButton.setCollapse();
        }
        else {
            taskItemsView.collapse();
            collapseButton.setExpand();
        }
    }


    private Handler.Callback callback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case ACTIVE_TASKS_UPDATED_MESSAGE_CODE:
                    tasksAdapter.notifyDataSetChanged();
                    collapseButton.updateTasksCount();
                    return true;
                default:
                    return false;
            }
        }
    };
}
