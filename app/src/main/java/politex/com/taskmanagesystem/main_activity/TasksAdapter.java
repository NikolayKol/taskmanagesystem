package politex.com.taskmanagesystem.main_activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.entities.Task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import politex.com.taskmanagesystem.R;
import politex.com.taskmanagesystem.TaskManageApp;

/**
 * Created by nikolay on 19.02.17.
 */

class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TaskViewHolder> {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("kk:mm", Locale.US);

    @Override
    public int getItemCount() {
        return TaskManageApp.getUseCases().getActiveTasks().size();
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        holder.update(position);
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_layout, parent, false);
        return new TaskViewHolder(view);
    }

    static class TaskViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time_text_view) TextView timeTextView;
        @BindView(R.id.task_text_view) TextView taskTextView;
        private Task task;

        TaskViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void update(int position) {
            task = TaskManageApp.getUseCases().getActiveTasks().get(position);
            String time = simpleDateFormat.format(new Date(task.getTime()));
            String taskText = TaskManageApp.getUseCases().getModel().getTaskText(task);
            timeTextView.setText(time);
            taskTextView.setText(taskText);
        }

        @OnClick(R.id.acknowledge_button)
        void acknowledge() {
            TaskManageApp.getUseCases().acknowledgeTask(task);
        }
    }
}
