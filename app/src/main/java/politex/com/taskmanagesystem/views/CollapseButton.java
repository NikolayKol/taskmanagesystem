package politex.com.taskmanagesystem.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import politex.com.taskmanagesystem.R;
import politex.com.taskmanagesystem.TaskManageApp;

/**
 * Created by nikolay on 19.02.17.
 */

public class CollapseButton extends FrameLayout {

    @BindView(R.id.tasks_count_text_view) TextView tasksCountTextView;
    @BindView(R.id.arrow_image_view) ImageView arrowImageView;

    public CollapseButton(Context context) {
        super(context);
        init(context);
    }

    public CollapseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CollapseButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void updateTasksCount() {
        String tasksCount = Integer.toString(TaskManageApp.getUseCases().getActiveTasks().size());
        tasksCountTextView.setText(tasksCount);
    }

    public void setExpand() {
        arrowImageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_white_24dp));
    }

    public void setCollapse() {
        arrowImageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_white_24dp));
    }



    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.collapse_button, this, true);
        ButterKnife.bind(this, view);
    }
}
