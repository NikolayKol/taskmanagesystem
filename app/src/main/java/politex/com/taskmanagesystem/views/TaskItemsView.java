package politex.com.taskmanagesystem.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.example.entities.TaskItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import politex.com.taskmanagesystem.R;
import politex.com.taskmanagesystem.TaskManageApp;

/**
 * Created by nikolay on 19.02.17.
 */

public class TaskItemsView extends FrameLayout {

    @BindView(R.id.persons_layout) LinearLayout personsLayout;
    @BindView(R.id.actions_layout) LinearLayout actionsLayout;
    @BindView(R.id.locations_layout) LinearLayout locationsLayout;

    @BindView(R.id.collapsed_layout) LinearLayout collapsedLayout;
    @BindView(R.id.expanded_layout) LinearLayout expandedLayout;

    @BindView(R.id.persons_scroll_view) HorizontalScrollView personsScrollView;
    @BindView(R.id.actions_scroll_view) HorizontalScrollView actionsScrollView;
    @BindView(R.id.locations_scroll_view) HorizontalScrollView locationsScrollView;

    @BindView(R.id.send_button) Button sendButton;

    public TaskItemsView(Context context) {
        super(context);
        init(context);
    }

    public TaskItemsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TaskItemsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public boolean isCollapsed() {
        return collapsedLayout.getVisibility() == VISIBLE;
    }

    public void collapse() {
        personsScrollView.setVisibility(GONE);
        actionsScrollView.setVisibility(GONE);
        locationsScrollView.setVisibility(GONE);

        expandedLayout.setVisibility(INVISIBLE);
        collapsedLayout.setVisibility(VISIBLE);
    }

    public void expand() {
        personsScrollView.setVisibility(VISIBLE);
        actionsScrollView.setVisibility(VISIBLE);
        locationsScrollView.setVisibility(VISIBLE);

        collapsedLayout.setVisibility(INVISIBLE);
        expandedLayout.setVisibility(VISIBLE);
    }

    @OnClick(R.id.reset_button)
    void reset() {
        int personsCount = personsLayout.getChildCount();
        for (int i = 0; i < personsCount; i++) {
            ((CheckBox) personsLayout.getChildAt(i)).setChecked(false);
        }

        int actionsCount = actionsLayout.getChildCount();
        for (int i = 0; i < actionsCount; i++) {
            ((CheckBox) actionsLayout.getChildAt(i)).setChecked(false);
        }

        int locationsCount = locationsLayout.getChildCount();
        for (int i = 0; i < locationsCount; i++) {
            ((CheckBox) locationsLayout.getChildAt(i)).setChecked(false);
        }
    }

    @OnClick(R.id.send_button)
    void sendClicked() {
        TaskManageApp.getUseCases().initTask(getCheckedPersonsIds(), getCheckedActionsIds(), getCheckedLocationsIds());
    }

    @OnClick(R.id.acknowledge_all_button)
    void acknowledgeAll() {
        TaskManageApp.getUseCases().acknowledgeAllTasks();
    }

    private List<String> getCheckedPersonsIds() {
        return getCheckedIds(personsLayout);
    }

    private List<String> getCheckedActionsIds() {
        return getCheckedIds(actionsLayout);
    }

    private List<String> getCheckedLocationsIds() {
        return getCheckedIds(locationsLayout);
    }

    private void isSendAvailable() {
        boolean sendAvailable = false;
        int personsCount = personsLayout.getChildCount();
        int actionsCount = actionsLayout.getChildCount();
        int locationsCount = locationsLayout.getChildCount();

        for (int i = 0; i < personsCount; i++) {
            sendAvailable |= ((CheckBox) personsLayout.getChildAt(i)).isChecked();
        }

        for (int i = 0; i < actionsCount; i++) {
            sendAvailable |= ((CheckBox) actionsLayout.getChildAt(i)).isChecked();
        }

        for (int i = 0; i < locationsCount; i++) {
            sendAvailable |= ((CheckBox) locationsLayout.getChildAt(i)).isChecked();
        }

        sendButton.setEnabled(sendAvailable);
    }

    private void init(Context context) {
        View view = inflate(context, R.layout.task_items_view, this);
        ButterKnife.bind(this, view);

        inflateItemsLayout(context, personsLayout, TaskManageApp.getUseCases().getModel().getPersons());
        inflateItemsLayout(context, actionsLayout, TaskManageApp.getUseCases().getModel().getActions());
        inflateItemsLayout(context, locationsLayout, TaskManageApp.getUseCases().getModel().getLocations());
    }

    private static List<String> getCheckedIds(LinearLayout linearLayout) {
        int childrenCount = linearLayout.getChildCount();

        List<String> ids = new ArrayList<>();

        for (int i = 0; i < childrenCount; i++) {
            CheckBox checkBox = (CheckBox) linearLayout.getChildAt(i);
            if (checkBox.isChecked())
                ids.add((String) checkBox.getTag());
        }

        return ids;
    }

    private void inflateItemsLayout(Context context, LinearLayout linearLayout, List<TaskItem> taskItems) {

        linearLayout.removeAllViews();

        for (TaskItem taskItem : taskItems) {
            CheckBox checkBox = (CheckBox) LayoutInflater.from(context).inflate(R.layout.task_item_checkbox, linearLayout, false);
            checkBox.setText(taskItem.getTitle());
            checkBox.setTag(taskItem.getId());
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    isSendAvailable();
                }
            });
            linearLayout.addView(checkBox);
        }
    }
}
