package politex.com.taskmanagesystem;

import android.app.Application;

import com.example.usecases.UseCases;

/**
 * Created by nikolay on 19.02.17.
 */

public class TaskManageApp extends Application {

    private static UseCases useCases;

    @Override
    public void onCreate() {
        super.onCreate();
        useCases = new UseCases(this);
    }

    public static UseCases getUseCases() {
        return useCases;
    }
}
