package com.example.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nikolay on 18.02.17.
 */

public class Model {

    private static final transient String DELIMITER = " --- ";
    private static final transient String SPACE = " ";

    @SerializedName("persons")
    private List<TaskItem> persons;

    @SerializedName("actions")
    private List<TaskItem> actions;

    @SerializedName("locations")
    private List<TaskItem> locations;

    public Model(List<TaskItem> persons, List<TaskItem> actions, List<TaskItem> locations) {
        this.persons = persons;
        this.actions = actions;
        this.locations = locations;
    }

    public List<TaskItem> getPersons() {
        return persons;
    }

    public List<TaskItem> getActions() {
        return actions;
    }

    public List<TaskItem> getLocations() {
        return locations;
    }

    public String getTaskText(Task task) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String id : task.getPersons()) {
            for (TaskItem person : persons) {
                if (person.getId().equals(id)) {
                    stringBuilder.append(person.getTitle());
                    stringBuilder.append(SPACE);
                }
            }
        }

        stringBuilder.append(DELIMITER);

        for (String id : task.getActions()) {
            for (TaskItem action : actions) {
                if (action.getId().equals(id)) {
                    stringBuilder.append(action.getTitle());
                    stringBuilder.append(SPACE);
                }
            }
        }

        stringBuilder.append(DELIMITER);

        for (String id : task.getLocations()) {
            for (TaskItem location : locations) {
                if (location.getId().equals(id)) {
                    stringBuilder.append(location.getTitle());
                    stringBuilder.append(SPACE);
                }
            }
        }

        return stringBuilder.toString();
    }
}
