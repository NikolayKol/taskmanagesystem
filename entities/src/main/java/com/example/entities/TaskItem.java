package com.example.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikolay on 18.02.17.
 */

public class TaskItem {

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private String id;

    public TaskItem(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
