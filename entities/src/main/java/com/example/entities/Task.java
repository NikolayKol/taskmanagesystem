package com.example.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nikolay on 18.02.17.
 */

public class Task {

    @SerializedName("id")
    private String id;

    @SerializedName("initiator_id")
    private String initiatorId;

    @SerializedName("time")
    private long time;

    @SerializedName("persons")
    private List<String> persons;

    @SerializedName("actions")
    private List<String> actions;

    @SerializedName("locations")
    private List<String> locations;

    public Task(String id, String initiatorId, long time, List<String> persons, List<String> actions, List<String> locations) {
        this.id = id;
        this.initiatorId = initiatorId;
        this.time = time;
        this.persons = persons;
        this.actions = actions;
        this.locations = locations;
    }

    public String getId() {
        return id;
    }

    public String getInitiatorId() {
        return initiatorId;
    }

    public long getTime() {
        return time;
    }

    public List<String> getPersons() {
        return persons;
    }

    public List<String> getActions() {
        return actions;
    }

    public List<String> getLocations() {
        return locations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (time != task.time) return false;
        if (id != null ? !id.equals(task.id) : task.id != null) return false;
        if (initiatorId != null ? !initiatorId.equals(task.initiatorId) : task.initiatorId != null)
            return false;
        if (persons != null ? !persons.equals(task.persons) : task.persons != null) return false;
        if (actions != null ? !actions.equals(task.actions) : task.actions != null) return false;
        return locations != null ? locations.equals(task.locations) : task.locations == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (initiatorId != null ? initiatorId.hashCode() : 0);
        result = 31 * result + (int) (time ^ (time >>> 32));
        result = 31 * result + (persons != null ? persons.hashCode() : 0);
        result = 31 * result + (actions != null ? actions.hashCode() : 0);
        result = 31 * result + (locations != null ? locations.hashCode() : 0);
        return result;
    }
}
